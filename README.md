# Hi there <img src="https://gitlab.com/awmir/awmir/-/raw/main/files/tde-coding.gif" width="29px">
<p align="center">

<a href="https://linkedin.com/in/qobadzadeh/" target="blank"><img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/linkedin.svg" alt="apoorvtyagi" height="20" width="20" /></a>&nbsp;

</p>

![](https://gitlab.com/awmir/awmir/-/raw/main/files/myBaseComing.gif)

### 🤵 About Me:
- 🏦 I'm currently working for a crypto currency exchange where i make financial applications using different Api's and designs
- 🤔 I use daily ```.js```,``` .bat```, ```.sql``` , ```.noSql``` <img src="https://gitlab.com/awmir/awmir/-/raw/main/files/hacker-hacker-man.gif" width="30">
- 💬 Talk to me about nodejs, various designs (both of these are not any kind of insect or animals) and silicon valley
- 😄 Pronouns: He/Him
- 📝 I regulary write articles on [linkedin](https://www.linkedin.com/in/qobadzadeh/)
- ⚡ Fun fact: Je connais un peu le français

<p align="center"/> 
<img src="https://gitlab.com/awmir/awmir/-/raw/main/files/Unofficial_JavaScript_logo_2.svg" width="35" height="60"/> 
<img src="https://gitlab.com/awmir/awmir/-/raw/main/files/Postgresql_elephant.svg" alt="c" width="40" height="40"/> 
<img src="https://gitlab.com/awmir/awmir/-/raw/main/files/Redis_Logo.svg"  width="140" height="45"/>
<img src="https://gitlab.com/awmir/awmir/-/raw/main/files/MongoDB_Logo.svg" width="160" height="90"/>
<img src="https://gitlab.com/awmir/awmir/-/raw/main/files/Elasticsearch_logo.svg" alt="kubernetes" width="150" height="70"/>
<img src="https://gitlab.com/awmir/awmir/-/raw/main/files/Git-logo.svg" alt="opencv" width="55" height="40"/> 
</p>



<!--START_SECTION:waka-->
**I'm a Night 🦉** 

```text
🌞 Morning    41 commits     ███░░░░░░░░░░░░░░░░░░░░░░   12.65% 
🌆 Daytime    83 commits     ██████░░░░░░░░░░░░░░░░░░░   25.62% 
🌃 Evening    172 commits    █████████████░░░░░░░░░░░░   53.09% 
🌙 Night      +250 commits   ██████████████████░░░░░░░   75.64%

```
📅 **I'm Most Productive on Tuesday** 

```text
Monday       55 commits     █████████████░░░░░░░░░░░░   50.71% 
Tuesday      23 commits     ████████████████░░░░░░░░░   65.82% 
Wednesday    25 commits     ███████████░░░░░░░░░░░░░░   45.5% 
Thursday     14 commits     ██████░░░░░░░░░░░░░░░░░░░   23.76% 
Friday       16 commits     █░░░░░░░░░░░░░░░░░░░░░░░░   5.44% 
Saturday     70 commits     ████████░░░░░░░░░░░░░░░░░   33.81% 
Sunday       91 commits     ██████████░░░░░░░░░░░░░░░   40.95%

```



<!--END_SECTION:waka-->

⏳ **Year Progress** { ███████████████████████████▁▁▁ } 90.17 % as on ⏰ 01-12-2021.


### 😜Here's a Joke for you:
<img src="https://readme-jokes.vercel.app/api" alt="Jokes Card" />


